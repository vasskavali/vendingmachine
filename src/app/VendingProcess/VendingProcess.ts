import {Injectable, Inject} from '@angular/core';
import{Currency} from './Currency';

@Injectable()    
export class VendingProcess{
    constructor () {}
    Inventory : any = [];
    DenominationTab : number[][];
    CountTable :number[][]
    coins : any = [25, 10, 5];
        CreateCashRegister():Currency[]{
            var curObj = new Currency();
            
            curObj.Denomination = 25;
            curObj.Count = 2;
            this.Inventory.push(curObj);

            curObj = new Currency();
            curObj.Denomination = 10;
            curObj.Count = 5;
            this.Inventory.push(curObj);

            curObj = new Currency();
            curObj.Denomination = 5;
            curObj.Count = 3;

            this.Inventory.push(curObj);

            return this.Inventory;
       }
       GetChange(inputAmt:number ): Currency[]{
             var temp : any = [];
           this.Inventory.forEach(element=>{
           if(element.Count > 0)    
            temp.push(this.coins.filter( x => x == element.Denomination)); 
        });
        //debugger;
           var bottomUpValue = this.minimumCoinBottomUp(inputAmt, temp);
         
           var output : Currency[];
           output = this.getCoinsfromInventory(this.Inventory, bottomUpValue, temp);
           return output;
       }

        printCoinCombination(R:number[], coins:number[]): Currency[] {
             var returncoins : Currency[] = new Array();
            if (R[R.length - 1] == -1) {
                return returncoins;
            }
            let start : number= R.length - 1;
            let i: number =0;
            while ( start != 0 ) {
                let j: number = R[start];
                var existingItem : Currency;
                 returncoins.forEach(function(x, ind){if(x.Denomination == coins[j]) existingItem = x;});
                if(existingItem != null){
                   existingItem.Count++;
                }else{
                    var curr = new Currency();
                    curr.Count = 1;
                    curr.Denomination = coins[j];
                    returncoins.push(curr);
                }  
                existingItem=null;             
                start = start - coins[j];
                i++;
            }
            return returncoins;
        }

        private minimumCoinBottomUp( total:number, coins:number[]): Currency[]{
        var T = new Array();
        var R = new Array();
        T[0] = 0;
        for(var i=1; i <= total; i++){
            T[i] = Number.MAX_SAFE_INTEGER-1;
            R[i] = -1;
        }
        for(var j=0; j < coins.length; j++){
            for(var i=1; i <= total; i++){
                if(i >= coins[j]){
                    if (T[i - coins[j]] + 1 < T[i]) {
                        T[i] = 1 + T[i - coins[j]];
                        R[i] = j;
                    }
                }
            }
        }

        return this.printCoinCombination(R, coins);
    }
     getCoinsfromInventory(inventory:Currency[], bottomUpValue: Currency[], coins:number[]): Currency[]{
            var _this : any = this;
            var output : Currency[] = new Array();
            var sum : number = 0;
            var bottomsum : number = 0;
            inventory.forEach(function(val, index){
               sum += val.Count; 
            });
            bottomUpValue.forEach(function(val, index){
               bottomsum += val.Count; 
            });
            if (bottomsum > sum)
            {
                return [];
            }
            else
            {
                //bottomUpValue.sort(function(a,b){return b.Denomination-a.Denomination});
                //debugger;
                bottomUpValue.forEach(function(item, index)
                {
                    var firstCoin : Currency;
                     inventory.forEach(element => {
                        if(element.Denomination == item.Denomination){ firstCoin = element;}
                    });
                    output.push(item);
                    if (firstCoin != null)
                    {
                        if (!(firstCoin.Count >= item.Count))
                        {
                            var tempCoins : any;
                            tempCoins = coins.filter( x => x != item.Denomination);
                            //debugger;
                            var remainingCoins = _this.minimumCoinBottomUp(item.Denomination * (item.Count - firstCoin.Count), tempCoins);
                            
                            output.forEach(function(x, ind){
                                if(x.Denomination == item.Denomination)
                                x.Count -= (item.Count - firstCoin.Count)});

                            remainingCoins.forEach(x =>
                            {
                                var tmp : Currency;
                                output.forEach(function(y,ind){
                                    if( y.Denomination == x.Denomination){
                                        tmp = y;}});
                                if (tmp != null) tmp.Count += x.Count;
                                else
                                    output.push(x);
                                    }
                            );
                            output = _this.getCoinsfromInventory(inventory, output, tempCoins);
                            }
                    }
                    else
                    {

                    }
                });
            }
            return output;
        }
}