import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpService{

  constructor(private http: Http) {}

  getInventory(): Observable<any> {
    return this.http.get('/app/mockdata/Inventory.json')
      .map(response => response.json());
  }
}