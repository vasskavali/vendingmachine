import {Component, OnInit, Inject} from '@angular/core';
import {HttpService} from '../HttpService/Http.Service';
import {VendingProcess} from '../VendingProcess/VendingProcess';
@Component ({
    selector:'vending-app',
    templateUrl:'./vending.html',
    styleUrls:['./vendingmachine.scss', './vendingmachine.css']
})
export class vendingComponent implements OnInit{
constructor(public httpService: HttpService, public vendingProcess: VendingProcess) {

}

Inventory: any;
CashRegister:any;
InsertedAmount: number = 0;
InsertedAmountString: string = "";

TotalSale: number = 0;
ErrorMsg : string = "";
GetItem (item){
    alert(item.itemdesc);
}
selectedItem: any ;
returnChange:any = [];
SelectedItem(Item:any){
    this.ErrorMsg = "";
    this.returnChange = [];
    if(this.InsertedAmount >0){
        if(this.InsertedAmount >= Math.floor(parseFloat(Item.itemcost)*100)){
            if((this.InsertedAmount - Math.floor(parseFloat(Item.itemcost)*100))!=0){
                debugger;
             var res = this.vendingProcess.GetChange(this.InsertedAmount - Math.floor(parseFloat(Item.itemcost)*100));
             if(res != null && res.length >0 ){
                this.returnChange = res;
                this.selectedItem = Item;
                this.InsertedAmount = 0; 
                this.InsertedAmountString = "";
                res.forEach(element => {
                    if(element.Count > 0)
                this.CashRegister.filter(x => x.Denomination == element.Denomination)[0].Count -= element.Count;              
                });
             }else{
                 this.ErrorMsg = "Insufficient Change.";
             }
            }else{
                this.selectedItem = Item;
                this.InsertedAmount = 0; 
                this.InsertedAmountString = "";
            }

        }else{
            this.ErrorMsg = "Insufficient Amount.Please insert more amount.";                
        }
    }else{
        this.ErrorMsg = "Please insert amount.";
    }
}
SaveInsertedAmount(amount:number){
    this.InsertedAmount += amount;
    this.InsertedAmountString = parseFloat((this.InsertedAmount/100).toString()).toFixed(2);
    if(amount != 100)
    this.CashRegister.filter(x => x.Denomination == amount)[0].Count++ ;
    
    this.returnChange = [];
    this.selectedItem = null;
    this.ErrorMsg = "";
}
public ngOnInit() {
    this.httpService.getInventory().subscribe(res=>{this.Inventory = res.Inventory;});
    this.CashRegister = this.vendingProcess.CreateCashRegister();
      
}
}